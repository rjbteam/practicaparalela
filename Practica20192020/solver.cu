#include <stdio.h>
#include <math.h>
#include <omp.h>
#include <sys/time.h>
#include "wtime.h"
#include "definitions.h"
#include "energy_struct.h"
#include "cuda_runtime.h"
#include "solver.h"

#define TAMBLOCK 128 
#define MAXBLOCK 65535

using namespace std;

/**
* Kernel del calculo de la solvation. Se debe anadir los parametros.
* Solucion en 2d donde hay un bloque por conformacion (hasta 65535 maximo). Cada hilo calcula un atomo del receptor contra 
* todos los atomos del ligando para una conformacion (bloque). En el eje x
* hay tantos bloques como atoms_r/TAMBLOCK y en el eje y hay tantos bloques como conformaciones. Si hay mas de 65535 conformaciones
* se reutilizan los bloques hasta completar el total de conformaciones.
*/
__global__ void escalculation (int atoms_r, int atoms_l, int nlig, float *rec_x_d, float *rec_y_d, float *rec_z_d, float *lig_x_d, float *lig_y_d, float *lig_z_d, float *ql_d,float *qr_d, float *energy_d, int nconformations) {
  double dist;
  int totalAtomLig = nconformations * nlig;

  __shared__ float energy_s[TAMBLOCK];

  //Conformacion a calcular: Numero de bloque.y teniendo en cuenta el maximo de 65535
  int conformation = 0;
  //Indice de x para los atomos del ligando
  int indexRec = threadIdx.x + blockIdx.x*blockDim.x;
  //Indice para identificar el primer ligando de bloque
  int indexLig = 0;
  int threadIndex = threadIdx.x;

  energy_s[threadIndex] = 0;

  
  if(indexRec < atoms_r) {
    //Para completar las conformaciones si nconformations > MAXBLOCK
    for(int k=0; k<nconformations; k+=MAXBLOCK) {
      //Para cada grupo de MAXBLOCK conformaciones se inicializa la shared
      energy_s[threadIndex] = 0;
      conformation = blockIdx.y+k;
      if(conformation < nconformations) {
        indexLig = (conformation)*atoms_l;

        for (int i=indexLig; i < indexLig+atoms_l; i++) {
          dist = calculaDistancia (rec_x_d[indexRec], rec_y_d[indexRec], rec_z_d[indexRec], lig_x_d[i], lig_y_d[i], lig_z_d[i]);
          //printf ("La distancia es %lf\n", dist);
          //El indice de ql_d debe calcularse
          energy_s[threadIndex] += (ql_d[i-indexLig]* qr_d[indexRec]) / dist;
          //printf ("La carga es %lf\n", total_elec);
        }

        __syncthreads();
  
        /*for(unsigned int s=1; s<blockDim.x; s*=2) {
          if (threadIndex % (2*s) == 0) {
            energy_s[threadIndex] += energy_s[threadIndex+s];
          }
        __syncthreads();
        }*/
    
        for (int s = blockDim.x / 2; s > 0; s >>= 1) {
          if (threadIndex < s) {
            energy_s[threadIndex] += energy_s[threadIndex + s];
          }
          __syncthreads();
        }

        if (threadIndex == 0)  {
          atomicAdd(&energy_d[conformation], energy_s[0]);
        }
      }//if nconformations
    }//for
  }//if atoms_r
}


/**
* Funcion para manejar el lanzamiento de CUDA 
*/
void forces_GPU_AU (int atoms_r, int atoms_l, int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql ,float *qr, float *energy, int nconformations){
	
	cudaError_t cudaStatus; //variable para recoger estados de cuda


	//seleccionamos device
	cudaSetDevice(0); //0 - Tesla K40 vs 1 - Tesla K230

	//creamos memoria para los vectores para GPU _d (device)
	float *rec_x_d, *rec_y_d, *rec_z_d, *qr_d, *lig_x_d, *lig_y_d, *lig_z_d, *ql_d, *energy_d;

	//reservamos memoria para GPU
  int recMemsize = atoms_r*sizeof(float);
  int ligMemsize = atoms_l*nconformations*sizeof(float);

  cudaError_t err;
  err = cudaMalloc((void **)&rec_x_d, recMemsize);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
    exit(-1);
  }

  err = cudaMalloc((void **)&rec_y_d, recMemsize);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
    exit(-1);
  }

  err = cudaMalloc((void **)&rec_z_d, recMemsize);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
    exit(-1);
  }

  err = cudaMalloc((void **)&lig_x_d, ligMemsize);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
    exit(-1);
  }

  err = cudaMalloc((void **)&lig_y_d, ligMemsize);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
    exit(-1);
  }

  err = cudaMalloc((void **)&lig_z_d, ligMemsize);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
    exit(-1);
  }

  err = cudaMalloc((void **)&qr_d, recMemsize);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
    exit(-1);
  }

  err = cudaMalloc((void **)&ql_d, atoms_l*sizeof(float));
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
   exit(-1);
  }

  err = cudaMalloc((void **)&energy_d, nconformations*sizeof(float));
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al reservar memoria en la GPU\n");
    exit(-1);
  }

	//pasamos datos de host to device
  err = cudaMemcpy(rec_x_d, rec_x, recMemsize, cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }	

  err = cudaMemcpy(rec_y_d, rec_y, recMemsize, cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

  err = cudaMemcpy(rec_z_d, rec_z, recMemsize, cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

  err = cudaMemcpy(lig_x_d, lig_x, ligMemsize, cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

  err = cudaMemcpy(lig_y_d, lig_y, ligMemsize, cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

  err = cudaMemcpy(lig_z_d, lig_z, ligMemsize, cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

  err = cudaMemcpy(qr_d, qr, recMemsize, cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

  err = cudaMemcpy(ql_d, ql, atoms_l*sizeof(float), cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

  err = cudaMemcpy(energy_d, energy, nconformations*sizeof(float), cudaMemcpyHostToDevice);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

	//Definir numero de hilos y bloques
  int total_hilos = atoms_r;
  int hilos_bloque = TAMBLOCK;
  int numBlockx = ceil(total_hilos/hilos_bloque)+1;
  int numBlocky = (nconformations > MAXBLOCK)? MAXBLOCK : nconformations;
  int dimBlockx = hilos_bloque;
  int dimBlocky = 1;
  printf("atoms_r=%d\n", total_hilos);
	printf("bloques: %d x %d = %d\n", (int)numBlockx, numBlocky, numBlockx*numBlocky);
	printf("hilos por bloque: %d x %d = %d\n", dimBlockx, dimBlocky, dimBlockx*dimBlocky);

  dim3 block(numBlockx, numBlocky);
  dim3 thread(dimBlockx, dimBlocky);

	//llamamos a kernel
	escalculation <<< block,thread>>> (atoms_r, atoms_l, nlig, rec_x_d, rec_y_d, rec_z_d, lig_x_d, lig_y_d, lig_z_d, ql_d, qr_d, energy_d, nconformations);
	
	//control de errores kernel
	cudaDeviceSynchronize();
	cudaStatus = cudaGetLastError();
	if(cudaStatus != cudaSuccess) fprintf(stderr, "Error en el kernel %d\n", cudaStatus); 

	//Traemos info al host
  err = cudaMemcpy(energy, energy_d, nconformations*sizeof(float), cudaMemcpyDeviceToHost);
  if(err != cudaSuccess) {
    fprintf(stderr, "Error al transferir informacion\n");
    exit(-1);
  }

	// para comprobar que la ultima conformacion tiene el mismo resultado que la primera
	printf("Termino electrostatico de conformacion %d es: %f\n", nconformations-1, energy[nconformations-1]); 

  /*for(int i=0; i<nconformations; i++) {
    printf("energy[%d]=%f\n", i, energy[i]);
  }*/

	//resultado varia repecto a SECUENCIAL y CUDA en 0.000002 por falta de precision con float
	//posible solucion utilizar double, probablemente bajara el rendimiento -> mas tiempo para calculo
	printf("Termino electrostatico %f\n", energy[0]);

	//Liberamos memoria reservada para GPU
  cudaFree(rec_x_d);
  cudaFree(rec_y_d);
  cudaFree(rec_z_d);
  cudaFree(lig_x_d);
  cudaFree(lig_y_d);
  cudaFree(lig_z_d);
  cudaFree(qr_d);
  cudaFree(ql_d);
  cudaFree(energy_d);
}

/**
* Distancia euclidea compartida por funcion CUDA y CPU secuencial
*/
__device__ __host__ extern float calculaDistancia (float rx, float ry, float rz, float lx, float ly, float lz) {

  float difx = rx - lx;
  float dify = ry - ly;
  float difz = rz - lz;
  float mod2x=difx*difx;
  float mod2y=dify*dify;
  float mod2z=difz*difz;
  difx=mod2x+mod2y+mod2z;
  return sqrtf(difx);
}




/**
 * Funcion que implementa el termino electrostático en CPU
 */
void forces_CPU_AU (int atoms_r, int atoms_l, int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql ,float *qr, float *energy, int nconformations){

	double dist, total_elec = 0, miatomo[3], elecTerm;
  int totalAtomLig = nconformations * nlig;

	for (int k=0; k < totalAtomLig; k+=nlig){
	  for(int i=0;i<atoms_l;i++){					
			miatomo[0] = *(lig_x + k + i);
			miatomo[1] = *(lig_y + k + i);
			miatomo[2] = *(lig_z + k + i);

			for(int j=0;j<atoms_r;j++){				
				elecTerm = 0;
        dist=calculaDistancia (rec_x[j], rec_y[j], rec_z[j], miatomo[0], miatomo[1], miatomo[2]);
//				printf ("La distancia es %lf\n", dist);
        elecTerm = (ql[i]* qr[j]) / dist;
				total_elec += elecTerm;
//        printf ("La carga es %lf\n", total_elec);
			}
		}
		
		energy[k/nlig] = total_elec;
		total_elec = 0;
  }
	printf("Termino electrostatico %f\n", energy[0]);
}


extern void solver_AU(int mode, int atoms_r, int atoms_l,  int nlig, float *rec_x, float *rec_y, float *rec_z, float *lig_x, float *lig_y, float *lig_z, float *ql, float *qr, float *energy_desolv, int nconformaciones) {

	double elapsed_i, elapsed_o;
	
	switch (mode) {
		case 0://Sequential execution
			printf("\* CALCULO ELECTROSTATICO EN CPU *\n");
			printf("**************************************\n");			
			printf("Conformations: %d\t Mode: %d, CPU\n",nconformaciones,mode);			
			elapsed_i = wtime();
			forces_CPU_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("CPU Processing time: %f (seg)\n", elapsed_o);
			break;
		case 1: //OpenMP execution
			printf("\* CALCULO ELECTROSTATICO EN OPENMP *\n");
			printf("**************************************\n");			
			printf("**************************************\n");			
			printf("Conformations: %d\t Mode: %d, CMP\n",nconformaciones,mode);			
			elapsed_i = wtime();
			forces_OMP_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("OpenMP Processing time: %f (seg)\n", elapsed_o);
			break;
		case 2: //CUDA exeuction
			printf("\* CALCULO ELECTROSTATICO EN CUDA *\n");
      printf("**************************************\n");
      printf("Conformaciones: %d\t Mode: %d, GPU\n",nconformaciones,mode);
			elapsed_i = wtime();
			forces_GPU_AU (atoms_r,atoms_l,nlig,rec_x,rec_y,rec_z,lig_x,lig_y,lig_z,ql,qr,energy_desolv,nconformaciones);
			elapsed_o = wtime() - elapsed_i;
			printf ("GPU Processing time: %f (seg)\n", elapsed_o);			
			break; 	
	  	default:
 	    	printf("Wrong mode type: %d.  Use -h for help.\n", mode);
			exit (-1);	
	} 		
}
